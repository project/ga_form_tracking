$(function() {
  $('form.ga-form-tracking input').blur(function() {
    var form = $(this).parents('form').attr('id');
    var url = '/' + Drupal.settings.ga_form_tracking.prefix + '/' + form + '/' + this.id;
    _gaq.push(['_trackPageview', url]);
  });
});
